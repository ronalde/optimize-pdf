# optimize-pdf

`optimize-pdf` is a bash (>=4) script to convert an existing PDF file
to screen (size) or archive optimized pdf.

The script supports command line arguments to optionally set metadata;
see [Usage](#usage).


## Rationale

I developed the script to make Office created PDF's usable for the web
by setting proper metadata and create usable formats; ie. a
screen-optimized version to display in (an iframe in) a webpage, and a
second one for download and archive purposes.


## Dependencies

Apart from bash (>=4) this script needs the following executables in
the path:

* `gs` (>=9.54)
  
  Tested with the this version packaged in arch which uses
  [ghostview/ghostscript](https://www.ghostscript.com/) source package.

  **NOTE**: in ghostscript version `9.54` some important
  [changes/improvements were introduced](https://www.ghostscript.com/doc/9.54.0/VectorDevices.htm#PDFA)
  regarding PDF/A handling and color conversion. At the time of
  writing Debian and Ubuntu based distributions use outdated versions
  which breaks `pdf-optimize`.
  
  See: https://repology.org/project/ghostscript/versions for all distributions.

* `pdfinfo`

  Tested with the `21.0.3` version packaged in arch which uses the 
  [poppler](https://poppler.freedesktop.org/) source package.


Using current Arch these demands can be satisfied using:

```bash
pacman -S ghostscript poppler
```


## Usage

* Create a screen/size-optimized PDF(1.4) version of the source file,
  storing the resulting file in a temporary directory
  (`/tmp/optimize-pdf-XXXX/sample.pdf`):

```bash
  ./optimize-pdf ~/documents/sample.pdf
```

* Idem but overwrite the source file:

```bash
  ./optimize-pdf --overwrite ~/documents/sample.pdf
```

* Create a new PDF(=1.7)/A(-2) version of the source PDF:

```bash
./optimize-pdf --target pdfa ~/documents/sample.pdf
```

* Idem, but make a PDF/A-3 file and set metadata in the specified target file:

```bash
./optimize-pdf --target pdfa3 --title "Archived PDF file" \
   --outputfile ~/documents/sample-pdfa.pdf ~/documents/sample.pdf
```

Use `./optimize-pdf --help` to see all supported arguments.

## Installation

Clone the project from gitlab to a local directory:

```bash
git clone https://gitlab.com/ronalde/optimize-pdf.git
cd optimize-pdf
./optimize-pdf --help
```

Or, you could run the script directly from gitlab, which some consider VERY UNSAFE:

```bash
bash <(wget -q -O - "https://gitlab.com/ronalde/optimize-pdf/-/raw/master/optimize-pdf") --help 

```


## Supported PDF and PDF/A specifications

Currently only the using the **b**asic archive specifications are supported. See [Limitations](#limitations).

* `--target pdfa1`\
   PDF/A-1b, a subset of PDF 1.4, 
* `--target pdfa`\
   or\
   `--target pdfa2`\
   PDF/A-2b, uses PDF 1.7
* `--target pdfa3`\
   PDF/ A-3B, uses PDF 1.7

The (non-PDF/A) `--target screen` returns a PDF 1.4 file.

### Limitations

Currently, ghostscript generated PDF's loose document structure, ie are
*non-tagged* and do not adhere to WCAG 2.0 Level A and AA. 

This means the following subsets/specs are not supported (yet?):

* PDF/UA (Universal Accessibility)
  * PDF/UA-1 = [ISO 14289](https://www.iso.org/standard/54564.html) 
  * PDF/UA-2 (based on PDF 2.0) = [ISO 14289-2](https://www.iso.org/standard/73127.html)
* PDF/A-[1,2,3]a are not supported (yet?)



### Additional information

#### Ghostscript PDF/A documentation

https://www.ghostscript.com/doc/9.54.0/VectorDevices.htm#PDFA


#### PDF/A validation

* **verapdf**

  A java based CLI and GUI validator: https://verapdf.org/
  
* https://www.pdfen.com/
 
  A web based validator (without hassle).

